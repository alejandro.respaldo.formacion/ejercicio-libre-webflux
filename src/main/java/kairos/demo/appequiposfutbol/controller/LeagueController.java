package kairos.demo.appequiposfutbol.controller;

import kairos.demo.appequiposfutbol.dto.LeagueDTO;
import kairos.demo.appequiposfutbol.dto.PlayerDTO;
import kairos.demo.appequiposfutbol.dto.TeamDTO;
import kairos.demo.appequiposfutbol.entities.League;
import kairos.demo.appequiposfutbol.entities.Player;
import kairos.demo.appequiposfutbol.entities.Team;
import kairos.demo.appequiposfutbol.services.LeagueService;
import kairos.demo.appequiposfutbol.utils.mappers.MappersFootballTeams;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/league/")
@AllArgsConstructor
public class LeagueController {

    private LeagueService leagueService;
    private MappersFootballTeams mapper;

    @GetMapping("/")
    public Flux<LeagueDTO> getAllLeagues() {
        return leagueService.findAllLeagues().map(mapper::mapToLeagueDTO);
    }

    @GetMapping("/nameLeague/{nameLeague}")
    public Flux<LeagueDTO> getLeaguesByName(@PathVariable String nameLeague) {
        var league = leagueService.findLeagueByName(nameLeague);
        return league.map(mapper::mapToLeagueDTO);
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<LeagueDTO> createLegue(@RequestBody Mono<LeagueDTO> leagueDTO) {
        System.out.println(leagueDTO);
        return leagueDTO.map(mapper::mapToLeague)
                .flatMap(leagueService::createLeague)
                .map(mapper::mapToLeagueDTO);
    }

    @PostMapping("/{idLeague}/newTeam")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<LeagueDTO> addTeamInLeague(@PathVariable String idLeague, @RequestBody Mono<TeamDTO> teamDTO) {
        var league = leagueService.findLeagueById(idLeague)
                .zipWith(teamDTO)
                .map(tuplaLeagueAndTeam -> insertTeam(tuplaLeagueAndTeam.getT1(), mapper.mapToTeam(tuplaLeagueAndTeam.getT2())))
                .flatMap(leagueService::createLeague);

        return league.map(mapper::mapToLeagueDTO);

    }

    @PostMapping("/{idLeague}/{nameTeam}/newPlayer")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<LeagueDTO> addPlayerInTeam(@PathVariable String idLeague, @PathVariable String nameTeam,
                                           @RequestBody Mono<PlayerDTO> playerDTO) {

        var league = leagueService.findLeagueById(idLeague)
                .zipWith(playerDTO)
                .map(tuplaLeagueAndNewPlayer -> insertNewPlayerInTeam(tuplaLeagueAndNewPlayer.getT1(),
                        mapper.mapToPlayer(tuplaLeagueAndNewPlayer.getT2()), nameTeam))
                .flatMap(leagueService::createLeague);

        return league.map(mapper::mapToLeagueDTO);
    }

    @GetMapping("/{teamName}")
    public Flux<LeagueDTO> findTeamByName(@PathVariable String teamName) {
        return leagueService.findTeamByName(teamName).map(mapper::mapToLeagueDTO);
    }

    private League insertTeam(League leagueInTupla, Team teamInTupla) {

        if (leagueInTupla != null) {
            if (leagueInTupla.getTeams() != null) {
                var teams = leagueInTupla.getTeams().stream().filter(t -> !t.getNameTeam().equalsIgnoreCase(teamInTupla.getNameTeam()))
                        .collect(Collectors.toList());

                teams.add(teamInTupla);
                leagueInTupla.setTeams(teams);
            } else {
                leagueInTupla.setTeams(new ArrayList<>());
                leagueInTupla.getTeams().add(teamInTupla);
            }
            return leagueInTupla;
        }

        return leagueInTupla;
    }

    private League insertNewPlayerInTeam(League league, Player player, String nameTeam) {

        if (league != null) {
            if (league.getTeams() != null) {

                var teams = league.getTeams().stream().collect(Collectors.toMap(Team::getNameTeam, team -> team));

                List<Player> players = null;

                if (teams.get(nameTeam).getPlayers() != null) {

                    players = teams.get(nameTeam).getPlayers().stream().filter(p ->
                            !p.getPlayerName().equals(player.getPlayerName())).collect(Collectors.toList());

                    players.add(player);

                } else {
                    players = new ArrayList<>();
                    players.add(player);
                }

                teams.get(nameTeam).setPlayers(players);

                List<Team> newTeams = new ArrayList<>();
                for (Team t : teams.values()) {
                    newTeams.add(t);
                }

                league.setTeams(newTeams);
            }
        }
        return league;
    }
}
