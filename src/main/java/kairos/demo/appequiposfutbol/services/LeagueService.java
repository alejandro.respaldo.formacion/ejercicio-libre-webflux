package kairos.demo.appequiposfutbol.services;

import kairos.demo.appequiposfutbol.entities.League;
import kairos.demo.appequiposfutbol.repository.LeagueRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class LeagueService {

    private LeagueRepository leagueRepository;

    public Flux<League> findAllLeagues(){
        return leagueRepository.findAll();
    }

    public Flux<League> findLeagueByName(String leagueName){
        return leagueRepository.findByLeagueName(leagueName);
    }

    public Mono<League> findLeagueById(String idLeague){
        return leagueRepository.findById(idLeague);
    }


    public Mono<League> createLeague(League league){
        return  leagueRepository.save(league);
    }

    public Flux<League> findTeamByName(String teamName){
        return leagueRepository.findByTeams_NameTeam(teamName);
    }

}
