package kairos.demo.appequiposfutbol.utils.mappers;

import kairos.demo.appequiposfutbol.dto.LeagueDTO;
import kairos.demo.appequiposfutbol.dto.PlayerDTO;
import kairos.demo.appequiposfutbol.dto.TeamDTO;
import kairos.demo.appequiposfutbol.entities.League;
import kairos.demo.appequiposfutbol.entities.Player;
import kairos.demo.appequiposfutbol.entities.Team;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MappersFootballTeams {

    public League mapToLeague(LeagueDTO leagueDTO) {

        List<Team> teams = null;
        if (leagueDTO.getTeams() != null) {
            teams = leagueDTO.getTeams().stream().map(this::mapToTeam).collect(Collectors.toList());
        }
        String idCreate = leagueDTO.getId() == null ? createIdByName(leagueDTO.getLeagueName()).concat(leagueDTO.getSeason())
                : leagueDTO.getId();

        return League.builder()
                .id(idCreate)
                .season(leagueDTO.getSeason())
                .country(leagueDTO.getCountry())
                .leagueName(leagueDTO.getLeagueName())
                .teams(teams)
                .build();
    }

    public LeagueDTO mapToLeagueDTO(League league) {
        System.out.println("-------mapper league to leaguedto " + league);
        List<TeamDTO> teamsDto = null;

        if (league.getTeams() != null) {
            teamsDto = league.getTeams().stream().map(this::mapToTeamDTO).collect(Collectors.toList());
        }
        return LeagueDTO.builder()
                .id(league.getId())
                .leagueName(league.getLeagueName())
                .country(league.getCountry())
                .teams(teamsDto)
                .season(league.getSeason())
                .build();
    }

    public Team mapToTeam(TeamDTO teamDTO) {
        List<Player> players = null;
        if (teamDTO.getPlayers() != null) {
            players = teamDTO.getPlayers().stream().map(this::mapToPlayer).collect(Collectors.toList());
        }

        String idCreate = teamDTO.getId() == null ? createIdByName(teamDTO.getNameTeam())
                : teamDTO.getId();

        return Team.builder()
                .id(idCreate)
                .nameTeam(teamDTO.getNameTeam())
                .players(players)
                .build();
    }

    public TeamDTO mapToTeamDTO(Team team){
        List<PlayerDTO> playersDto = null;
        if(team.getPlayers() != null){
            playersDto = team.getPlayers().stream().map(this::mapToPlayerDTO).collect(Collectors.toList());
        }

        return TeamDTO.builder()
                .id(team.getId())
                .nameTeam(team.getNameTeam())
                .players(playersDto)
                .build();

    }

    public Player mapToPlayer(PlayerDTO playerDTO) {
        String idPlayer = playerDTO.getId() == null ? createIdByName(playerDTO.getPlayerName())
                : playerDTO.getId();

        return Player.builder()
                .id(idPlayer)
                .playerName(playerDTO.getPlayerName())
                .playerNumber(playerDTO.getPlayerNumber())
                .position(playerDTO.getPosition())
                .build();
    }

    public PlayerDTO mapToPlayerDTO(Player player){
        return PlayerDTO.builder()
                .id(player.getId())
                .playerName(player.getPlayerName())
                .playerNumber(player.getPlayerNumber())
                .position(player.getPosition())
                .build();
    }

    private String createIdByName(String name) {
        String id = "";
        if (name != null) {
            var ide = Arrays.asList(name.split(" ")).stream().map(n -> {
               var charA =  n.charAt(0);
               var cahrB = n.charAt(n.length()-1);
               return String.valueOf(charA).concat(String.valueOf(cahrB));
            }).collect(Collectors.toList());
            for (String c : ide){
                id += c;
            }
        }
        return id;
    }
}
