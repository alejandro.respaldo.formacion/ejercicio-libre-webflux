package kairos.demo.appequiposfutbol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppEquiposFutbolApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppEquiposFutbolApplication.class, args);
	}

}
