package kairos.demo.appequiposfutbol.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class League {

    @Id
    private String id;
    private String leagueName;
    private List<Team> teams;
    private String season;
    private String country;

}
