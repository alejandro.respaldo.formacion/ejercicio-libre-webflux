package kairos.demo.appequiposfutbol.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PlayerDTO {

    @Id
    private String id;
    private Integer playerNumber;
    private String playerName;
    private String position;
}

