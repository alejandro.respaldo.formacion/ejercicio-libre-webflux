package kairos.demo.appequiposfutbol.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TeamDTO {

    @Id
    private String id;
    private String nameTeam;
    private List<PlayerDTO> players;

}
