package kairos.demo.appequiposfutbol.repository;

import kairos.demo.appequiposfutbol.entities.League;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface LeagueRepository extends ReactiveMongoRepository<League, String> {
    Flux<League> findByTeams_NameTeam(String nameTeam);


    Flux<League> findByLeagueName(String leagueName);

}


