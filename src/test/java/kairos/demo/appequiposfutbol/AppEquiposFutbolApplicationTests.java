package kairos.demo.appequiposfutbol;

import kairos.demo.appequiposfutbol.dto.LeagueDTO;
import kairos.demo.appequiposfutbol.dto.PlayerDTO;
import kairos.demo.appequiposfutbol.dto.TeamDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AppEquiposFutbolApplicationTests {

	@Autowired
	private WebTestClient webTestClient;

	@Test
	void createNewLeague() {
		LeagueDTO league = new LeagueDTO(null, "Premier League", "22-23","England",null);

		webTestClient.post().uri("/league/")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(league),LeagueDTO.class)
				.exchange()
				.expectStatus().isCreated()
				.expectHeader().contentType(MediaType.APPLICATION_JSON)
				.expectBody()
				.jsonPath("$.id").isNotEmpty()
				.jsonPath("$.id").isEqualTo("PrLe22-23");
	}

	@Test
	void createTeamInLeague(){
		TeamDTO team = new TeamDTO(null,"Manchester United",null);
		webTestClient.post().uri("/league/PrLe22-23/newTeam")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(team), TeamDTO.class)
				.exchange()
				.expectStatus().isCreated()
				.expectHeader().contentType(MediaType.APPLICATION_JSON)
				.expectBody()
				.jsonPath("$.teams[0].id").isNotEmpty()
				.jsonPath("$.teams[0].id").isEqualTo("MrUd");
	}

	@Test
	void createPlayerInTeam(){
		PlayerDTO player = new PlayerDTO(null,9,"Carlos Casimiro","Defensive midfielder");
		webTestClient.post().uri("/league/PrLe22-23/Manchester United/newPlayer")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.body(Mono.just(player), TeamDTO.class)
				.exchange()
				.expectStatus().isCreated()
				.expectHeader().contentType(MediaType.APPLICATION_JSON)
				.expectBody()
				.jsonPath("$.teams[0].players[0].id").isNotEmpty()
				.jsonPath("$.teams[0].players[0].id").isEqualTo("CsCo");
	}

}
